//
// Created by zsolt on 2017.09.28..
//

#ifndef OAF_1_APPLICATION_H
#define OAF_1_APPLICATION_H


#include "Sack.h"

class Application {
public:
    Sack sack;

    void run(Sack &s);

private:
    void add(Sack &s);

    void remove(Sack &s);

    void print(Sack &s);

    void unite(Sack &s);

    void writeMenu();
};


#endif //OAF_1_APPLICATION_H
