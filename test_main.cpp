//
// Created by zsolt on 2017.09.28..
//

#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "Sack.h"

TEST_CASE("general") {
    CHECK_NOTHROW([&]() {
        Sack sack;
        sack.add(1).add(3).add(3).add(2).remove(3).add(5).add(5).remove(5);

        auto *sack2 = new Sack(sack);
        auto sack3 = sack;

        sack2->remove(5);
        sack3.remove(5).remove(2);

        CHECK(sack[0].val == 1);
        CHECK(sack[1].val == 3);
        CHECK(sack[2].val == 2);
        CHECK(sack[3].val == 5);

        CHECK(sack2->at(0).val == 1);
        CHECK(sack2->at(1).val == 3);
        CHECK(sack2->at(2).val == 2);

        CHECK(sack3[0].val == 1);
        CHECK(sack3[1].val == 3);
    });
}

TEST_CASE("input from file") {
    std::ifstream i("input.txt");
    if (i.fail()) {
        std::cerr << "Failed to open input file!" << std::endl;
    }

    Sack sack(5);

    i >> sack;

    CHECK(sack[0].val == 9);
    CHECK(sack[1].val == 8);
    CHECK(sack[2].val == 7);
    CHECK(sack[3].val == 6);
    CHECK(sack[4].val == 5);
}

TEST_CASE("empty sack") {
    Sack sack;
    CHECK(sack.isEmpty());
}

TEST_CASE("remove then add") {
    Sack sack;
    sack.add(1).remove(1).add(2);

    CHECK(sack.count(1) == 0);
    CHECK(sack.count(2) == 1);
}

TEST_CASE("counting") {
    Sack sack(10);
    sack.add(1).add(1).add(1).add(1);
    sack.add(2).add(2).add(2);
    sack.add(3).add(3);
    sack.add(4);

    CHECK(sack.count(1) == 4);
    CHECK(sack.count(2) == 3);
    CHECK(sack.count(3) == 2);
    CHECK(sack.count(4) == 1);
}

TEST_CASE("union of sacks") {
    Sack sack;
    sack.add(1).add(1).add(1).add(1);
    sack.add(2).add(2).add(2);
    sack.add(3).add(3);
    sack.add(4);
    sack.add(5);

    Sack sack2;
    sack2.add(4).add(4).add(4).add(4);
    sack2.add(3).add(3).add(3);
    sack2.add(2).add(2);
    sack2.add(1);
    sack2.add(-128);

    Sack sack3 = sack.unite(sack2);
    CHECK(sack3.count(1) == 1);
    CHECK(sack3.count(2) == 2);
    CHECK(sack3.count(3) == 2);
    CHECK(sack3.count(4) == 1);
    CHECK(sack3.count(5) == 1);
    CHECK(sack3.count(-128) == 1);
}

TEST_CASE("Exceptions") {
    Sack sack;
    sack.add(1).remove(1);

    CHECK_THROWS(sack.remove(1));
    CHECK_THROWS(sack[2]);
}