//
// Created by zsolt on 2017.09.28..
//

#include <iostream>
#include <algorithm>
#include "Sack.h"

// Constructor
Sack::Sack(unsigned int size) : size(size) {
    p = new Storage[size];
    index = 0;
}

// Copy constructor
Sack::Sack(const Sack &s) {
    size = s.size;
    p = new Storage[size];

    for (index = 0; index < size; ++index) {
        p[index] = s[index];
    }
}

// Destructor
Sack::~Sack() {
    delete[] p;
}

Sack &Sack::add(int x) {
    unsigned int i = 0;
    bool found = false;

    while (!found && i < size) {
        found = p[i].val == x;
        ++i;
    }

    if (found) {
        ++(p[i - 1].count);
    } else {
        if (size == index) {
            ++size;
            resize(size);
        }

        p[index++] = Storage(x);
    }

    return *this;
}

Sack &Sack::remove(int x) {
    unsigned int i = 0;
    bool found = false;

    while (!found && i < size) {
        found = p[i].val == x;
        ++i;
    }

    if (found) {
        if (p[i - 1].count == 1) {
            auto tmp = p[i - 1];

            p[i - 1] = p[size];
            p[size] = tmp;

            --index;
            resize(--size);
        } else {
            --(p[i - 1].count);
        }
    } else {
        throw Exception::SACK_DOES_NOT_CONTAIN;
    }

    return *this;
}

void Sack::resize(unsigned int size) {
    auto *n = new Storage[size];
    for (unsigned int i = 0; i < size; ++i) {
        n[i] = p[i];
    }

    delete[] p;
    p = n;
}

unsigned int Sack::count(int x) {
    unsigned int i = 0;
    bool found = false;

    while (!found && i < size) {
        found = p[i].val == x;
        ++i;
    }

    if (found) {
        return p[i - 1].count;
    }

    return 0;
}

std::ostream &operator<<(std::ostream &os, const Sack &sack) {
    for (unsigned int i = 0; i < sack.size; ++i) {
        std::cout << sack.p[i].val << ":" << sack.p[i].count << " ";
    }

    return os;
}

std::istream &operator>>(std::istream &is, Sack &sack) {
    for (unsigned int i = 0; i < sack.size; ++i) {
        int x;
        is >> x;
        sack.add(x);
    }

    return is;
}

Storage &Sack::at(unsigned int i) {
    if (i < size) {
        return p[i];
    }

    throw Exception::INDEX_OUT_OF_BOUNDS;
}

Storage Sack::at(unsigned int i) const {
    if (i < size) {
        return p[i];
    }

    throw Exception::INDEX_OUT_OF_BOUNDS;
}

Storage &Sack::operator[](unsigned int i) {
    return at(i);
}

Storage Sack::operator[](unsigned int i) const {
    return at(i);
}

Sack &Sack::operator=(const Sack &sack) {
    if (&sack == this) {
        return *this;
    }

    delete[] p;

    size = sack.size;
    p = new Storage[size];

    for (unsigned int i = 0; i < size; ++i) {
        p[i] = sack[i];
    }

    return *this;
}

bool Sack::isEmpty() const {
    return size == 0;
}

Sack &Sack::unite(const Sack &sack) {
    auto *res = new Sack();
    auto func = [](const Storage &a, const Storage &b) -> bool {
        return a.val < b.val;
    };

    std::sort(begin(), end(), func);
    std::sort(sack.begin(), sack.end(), func);

    auto it1 = begin();
    auto it2 = sack.begin();

    // handle <,>,=
    while (it2 != sack.end() && it1 != end()) {
        if (it1->val < it2->val) {
            res->addTimes(it1->count, it1->val);
            ++it1;

        } else if (it1->val > it2->val) {
            res->addTimes(it2->count, it2->val);
            ++it2;

        } else {
            // if =, choose the smaller
            if (it1->count <= it2->count || it2->count == 0) {
                res->addTimes(it1->count, it1->val);
            } else {
                res->addTimes(it2->count, it2->val);
            }

            ++it1;
            ++it2;
        }
    }

    // add the remaining of the larger array
    if (it1 == end()) {
        while (it2 != sack.end()) {
            res->addTimes(it2->count, it2->val);
            ++it2;
        }
    } else if (it2 == sack.end()) {
        while (it1 != end()) {
            res->addTimes(it1->count, it1->val);
            ++it1;
        }
    }

    return *res;
}

Sack &Sack::addTimes(unsigned int times, int value) {
    for (unsigned int i = 0; i < times; ++i) {
        add(value);
    }

    return *this;
}
