//
// Created by zsolt on 2017.09.28..
//

#ifndef OAF_1_SACK_H
#define OAF_1_SACK_H

#include <ostream>

struct Storage {
    int val;
    unsigned int count = 1;

    explicit Storage(int x = 0) : val(x) {};
};

class Sack {
public:
    enum class Exception {
        INDEX_OUT_OF_BOUNDS,
        SACK_DOES_NOT_CONTAIN
    };

    explicit Sack(unsigned int size);

    Sack() : Sack(0) {};

    Sack(const Sack &s);

    ~Sack();

    Sack &add(int x);

    Sack &addTimes(unsigned int times, int value);

    Sack &remove(int x);

    bool isEmpty() const;

    Sack &unite(const Sack &sack);

    unsigned int count(int x);

    Sack &operator=(const Sack &sack);

    Storage &at(unsigned int i);

    Storage at(unsigned int i) const;

    Storage &operator[](unsigned int i);

    Storage operator[](unsigned int i) const;

    friend std::ostream &operator<<(std::ostream &os, const Sack &sack);

    friend std::istream &operator>>(std::istream &is, Sack &sack);

    Storage *begin() const { return &p[0]; };

    Storage *end() const { return &p[size]; };

private:
    Storage *p;
    unsigned int size;
    unsigned int index;

    void resize(unsigned int size);
};

#endif //OAF_1_SACK_H
