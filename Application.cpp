//
// Created by zsolt on 2017.09.28..
//

#include <iostream>
#include "Application.h"

void Application::run(Sack &s) {
    int n = 0;

    do {
        writeMenu();
        std::cin >> n;

        switch (n) {
            case 1:
                add(s);
                break;
            case 2:
                remove(s);
                break;
            case 3:
                unite(s);
                break;
            case 4:
                print(s);
                break;

            default:
                break;
        }
    } while (n != 0);
}

void Application::add(Sack &s) {
    std::cout << "Add number to sack: ";
    int n;
    std::cin >> n;
    s.add(n);

}

void Application::remove(Sack &s) {
    std::cout << "Remove number from sack: ";
    int n;
    std::cin >> n;

    try {
        s.remove(n);
    } catch (Sack::Exception e) {
        std::cout << "The sack does not contain that number!" << std::endl;
    }
}

void Application::print(Sack &s) {
    std::cout << s << std::endl;
}

void Application::unite(Sack &s) {
    Sack s2;

    std::cout << "Edit the other sack!" << std::endl;
    run(s2);

    sack = sack.unite(s2);
}

void Application::writeMenu() {
    using namespace std;

    cout << "0 - exit" << endl;
    cout << "1 - add number" << endl;
    cout << "2 - remove number" << endl;
    cout << "3 - make union" << endl;
    cout << "4 - print sack" << endl;
}
